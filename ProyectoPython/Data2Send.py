import time
import datetime

class Data2Send:

    def __init__(self, j2p):
        self.j2p = j2p

    def update(self, arrayDict, timeStamp):
        timeDict = { 'TimeStamp': timeStamp}
        arrayDict.insert(0, timeDict)
        self.j2p.toJson(arrayDict, timeStamp)
