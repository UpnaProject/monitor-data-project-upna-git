from Component import Component
import cv2


class Button(Component):

    def __init__(self, dataComp):
        super().__init__(dataComp)


    def ini(self, *args, **kwargs):
        self.hsvROI = cv2.cvtColor(super().__createROI__(*args), cv2.COLOR_BGR2HSV)
        self.__initStateCalculation__(self.hsvROI, super().__getColor__())


    def __initStateCalculation__(self, hsvIniRoi, colorRange):
        self.originalMask = cv2.inRange(hsvIniRoi, colorRange[0], colorRange[1])
        if len(colorRange)>2:
            extendedMask = cv2.inRange(hsvIniRoi, colorRange[2], colorRange[3])
            self.originalMask = cv2.add(self.originalMask, extendedMask)

        self.originalMask = 255 - self.originalMask

        _,contours,_ = cv2.findContours(self.originalMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        if len(contours) != 0:
            maxiArea = max(contours, key = cv2.contourArea)
            x,y,w,h = cv2.boundingRect(maxiArea)
            miniRoi = self.originalMask[y:y+h, x:x+w]
            self.originalMaxiAreaZeros = cv2.countNonZero(miniRoi)
        else:
            self.originalMaxiAreaZeros = 0


    def getResult(self):
        return 'Activo' if self.stateOn == True else 'Inactivo'


    def update(self, *args, **kwargs):
        currentHsvROI = cv2.cvtColor(super().__createROI__(*args), cv2.COLOR_BGR2HSV)
        self.colorCalculate(self.hsvROI,currentHsvROI, super().__getColor__())


    def colorCalculate(self, hsvROI, currentHsvROI, colorRange):
        self.mask = cv2.inRange(currentHsvROI, colorRange[0], colorRange[1])
        if len(colorRange)>2:
            extendedMask = cv2.inRange(currentHsvROI, colorRange[2], colorRange[3])
            self.mask = cv2.add(self.mask, extendedMask)

        self.mask = 255 - self.mask

        _,contours,_ = cv2.findContours(self.mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        if len(contours) != 0:
            maxiArea = max(contours, key = cv2.contourArea)
            x,y,w,h = cv2.boundingRect(maxiArea)
            miniRoi = self.mask[y:y+h, x:x+w]
            maxiAreaZeros = cv2.countNonZero(miniRoi)

            if (self.originalMaxiAreaZeros*0.7 > maxiAreaZeros or self.originalMaxiAreaZeros*1.3 < maxiAreaZeros ):
                self.stateOn = not self.stateOn
                self.originalMaxiAreaZeros = maxiAreaZeros
                self.originalMask = self.mask
