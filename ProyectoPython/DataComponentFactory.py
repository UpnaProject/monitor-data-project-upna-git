from DataComponent import DataComponent

class DataComponentFactory():
    
    def __init__(self, jsonAlreadyProcessed):
        
        self.dataComponentList = []
        for components in jsonAlreadyProcessed:
            self.dataComponentList.append(DataComponent(components.Color, components.Coordenadas, components.Name, components.State, components.Type))
            
                    ## Para Debug ##
        for row in self.dataComponentList:
            print("El componente es de tipo {} y se ha etiquetado como {}".format(row.tipoComp,row.nombreComp))
