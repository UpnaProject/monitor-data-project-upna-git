## Interface Observer ##
"""
Cada objeto(instancia) que implemente esta interface/clase será notificado para su actualización
por otra clase externa.
"""
class IObserver():
    
    def update(self, *args, **kwargs):
        pass   