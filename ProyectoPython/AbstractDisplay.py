from Component import Component
import cv2
from PIL import Image
from abc import ABCMeta, abstractmethod

class AbstractDisplay(Component):

    __metaclass__ = ABCMeta

    def __init__(self, dataComp):
        super().__init__(dataComp)

    @abstractmethod
    def ini(self, *args, **kwargs): pass

    def __update__(self, *args, **kwargs):
        super().__createROI__(*args)

        self.roiG = cv2.cvtColor(self.roi, cv2.COLOR_BGR2GRAY)
        self.roiG = cv2.GaussianBlur(self.roiG, (5, 5), 0)
        im2Pil = Image.fromarray(self.roiG)
        return im2Pil
    
    def __getRoi__(self, *args, **kwargs):
        return super().__createROI__(*args)
    
    def __getImageFromROI__(self, *args, **kwargs):
        cv2.cvtColor(*args, cv2.COLOR_BGR2GRAY)
        cv2.GaussianBlur(*args, (5, 5), 0)
        im2Pil = Image.fromarray(*args)
        return im2Pil