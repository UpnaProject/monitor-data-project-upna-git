import numpy as np
import cv2

class Homography:

    def __init__(self, pathMedia):
        
        self.imgname = "cropCapture.jpg"
        self.MIN_MATCH_COUNT = 20
        self.img1 = cv2.imread(pathMedia + self.imgname)
        gray1 = cv2.cvtColor(self.img1, cv2.COLOR_BGR2GRAY)

        self.orb = cv2.ORB_create()
        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
        self.kpts1, self.descs1 = self.orb.detectAndCompute(gray1,None)
        self.iter = 0
        self.im_out = None

    def update(self, entryFrame, **kwargs):
        gray2 = cv2.cvtColor(entryFrame, cv2.COLOR_BGR2GRAY)
        kpts2, descs2 = self.orb.detectAndCompute(gray2,None)

        try:
            matches = self.bf.match(self.descs1, descs2)
            dmatches = sorted(matches, key = lambda x:x.distance)
            print('Matches encontrados ORB+ORB', len(dmatches))
            if len(dmatches) > self.MIN_MATCH_COUNT:

                src_pts  = np.float32([self.kpts1[m.queryIdx].pt for m in dmatches]).reshape(-1,1,2)
                dst_pts  = np.float32([kpts2[m.trainIdx].pt for m in dmatches]).reshape(-1,1,2)

                M, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC,5.0)

                self.im_out = cv2.warpPerspective(entryFrame, M, (self.img1.shape[1],self.img1.shape[0]))

            else:
                ## No hay suficientes matches para considerar que es lo suficientemente robusto,
                ## así que hay que empezar a buscar con el frame siguiente
                print('Los Matches no llegan al mínimo requerido')
        except:
            print('Matches nulos. Se pierde el frame y se mantiene la homografía anterior')
        