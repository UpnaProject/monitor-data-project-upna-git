## Interface ISubject ##
"""
Cada objeto(instancia) que implemente esta interface/clase emite una notificación 
que llegará a todos los objetos IObserver que estén registrados en su lista.
"""

class ISubject():
        
    def __init__(self):
        self.observers = []
 
    def register(self, observer):
        if not observer in self.observers:
            self.observers.append(observer)
 
    def unregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)
 
    def unregister_all(self):
        if self.observers:
            del self.observers[:]
 
    def update_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)
            
    def ini_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.ini(*args, **kwargs)