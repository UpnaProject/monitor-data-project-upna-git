class FirstRun:
    
    
    def __init__(self, jsonHandler, pathJson):
        self.jsonHandler = jsonHandler
        self.pathJson= pathJson

        
    def setFirsRun(self):
        self.__firstRun__()
        
    def setSecondRun(self):
        self.__secondRun__()        
        
    def __firstRun__(self):
        boolLoop1 = False
        while not boolLoop1:
            try:
               self.jsonHandler.jsonLoad(self.pathJson +'puntos.json')
               boolLoop1 = True
            except:
                print("No se puede leer el archivo puntos.json. Comprueba que existe en el root")
                
        print('ROI principal configurado. A la espera del archivo regiones.json')
        
    def __secondRun__(self):
        boolLoop2 = False
        while not boolLoop2:
     
            try:
               self.jsonHandler.jsonLoads(open(self.pathJson + 'regiones.json').read())
               boolLoop2 = True
            except:
                print("No se puede leer el archivo regiones.json. Comprueba que existe en el root")
                
        print('regiones.json leido y validado. Se inicia el proceso de an?lisis')       
        