import numpy as np
import cv2
from globalsProject import constants

class CalibrationHelper():
    
    def __init__(self):
        self.mtx = np.array(constants.MTX)
        self.dist = np.array(constants.DIST)

    def getMtx(self):
        return self.mtx
    
    def getDist(self):
        return self.dist
    
    def getUndistortedImage(self, imageToUndistort):
        print('Undistortion applied')
        dst = cv2.undistort(imageToUndistort, self.getMtx(), self.getDist(), None, self.getMtx())
        return dst
