import keras
from keras.models import load_model
import numpy as np


class NeuronalNetwork: 
    def __init__(self, nameModel):
        try:
            print('keras:{}'.format(keras.__version__))
        except:
            raise Exception("Keras is not loaded correctly. Check wheter a compatible version of Keras is installed")

        self.neuronalModel = self.__load_model__(nameModel)

    def __load_model__(self, nameModel):
        try:
            model = load_model(nameModel)
        except ImportError as error:
            raise error
        return model

    def __openData__(self, file2open):
        with open(file2open) as f:
            array = []
            for line in f:
                array.append(line)    
        return array
    
    def getInfoTest(self):
        self.neuronalModel.summary()

    def predictResults(self, images2Predict):
        # Predecimos el resultado
        normalized_data = images2Predict / 255
        flatten_image = np.reshape(normalized_data, (1, 784))
        dataPredicted = self.neuronalModel.predict_classes(flatten_image)
        return dataPredicted
