import os
import sys

""" Relatives Paths """
RESOURCES = '/Resources/'
HELPERS = '/Helpers/'

""" EXAMPLE PATHS FOR RASPBERRY  """ ## This should be removed after the CUSTOM PATHS is defined ##
INPUT_JSON= '/var/www/html/upload/'
MEDIA = '/var/www/html/media/'
SAVING_DEBUG_ROIS = '/var/www/html/media/'
SAVING_CROPPED_NUMBERS = '/var/www/html/media/croppedNum'
SAVING_CROPPED_ROIS = '/var/www/html/media/croppedROI'

""" CUSTOM PATHS """ ## INTRODUCE Here the paths for the current project ##
# INPUT_JSON =
# MEDIA = 
# SAVING_DEBUG_ROIS =
# SAVING_CROPPED_NUMBERS = 
# SAVING_CROPPED_ROIS = 