from IObserver import IObserver
import numpy as np

class Component(IObserver):

    def __init__(self, dataComp):
        self.dataComp = dataComp
        # Cada Componente viene definido por los siguientes parámetros: #
        """
        Posición. Dos pares de coordenadas {[x1, y1], [x2, y2]} delimitan el rectángulo en el que se encuentra el componente
        Nombre. String introducido por el usuario que sirve para la identificación del componente analizado.
        Color. String que indica el color predominante y que será usado en el algoritmo de procesado de imagen. Puede ser Null.
        Tipo. Indica el subtipo de componente. Es cada una de las clases que deriva de esta.
        State. Estado (boolean) que indica si el componente está Activo/Pulsado. Usado en componentes tipo Botón...
                Botón a True --> Encendido. Botón a False --> Apagado
        """

        self.__checkCoordenadas__(dataComp)

        self.width = self.x2 - self.x1
        self.height = self.y2 - self.y1
        self.color = dataComp.colorComp
        self.name = dataComp.nombreComp
        self.stateOn = dataComp.stateComp

        self.roi = []
        self.colorRange = []
        self.__checkColor__()

    def __checkCoordenadas__(self, dataComp):
        if (dataComp.coordComp[0] < dataComp.coordComp[2]):
            self.x1 = int(dataComp.coordComp[0])
            self.x2 = int(dataComp.coordComp[2])
        else:
            self.x1 = int(dataComp.coordComp[2])
            self.x2 = int(dataComp.coordComp[0])
        if (dataComp.coordComp[1] < dataComp.coordComp[3]):
            self.y1 = int(dataComp.coordComp[1])
            self.y2 = int(dataComp.coordComp[3])
        else:
            self.y1 = int(dataComp.coordComp[3])
            self.y2 = int(dataComp.coordComp[1])

    def __createROI__(self, frameCapturado):
        self.roi = frameCapturado[self.y1:self.y2, self.x1:self.x2]
        return self.roi

    def __checkColor__(self):
        if self.color is None:
            return

        colorPick = self.color
        chartColor = {'GREEN': [[49,50,50], [88, 255, 255]],
                      'RED': [[0,65,75], [12, 255, 255],[170,65,75], [180, 255, 255]],
                      'BLUE': [[92,65,75],[130, 255, 255]],
                      'YELLOW': [[23,150,211], [35, 255, 255]],
                      'ORANGE': [[13,142,242], [22, 255, 255]],
                      'BLACK': [[0,0,0], [180, 255, 30]],
                      'WHITE': [[0,0,255], [180, 0, 225]],
                      }

        if colorPick in chartColor:
            colorBase = chartColor[colorPick]

            lowColor = np.array(colorBase[0], dtype = np.uint8)
            highColor = np.array(colorBase[1], dtype = np.uint8)
            self.colorRange.append(lowColor)
            self.colorRange.append(highColor)
            if colorPick == 'RED':
                lowColor2 = np.array(colorBase[2], dtype = np.uint8)
                highColor2 = np.array(colorBase[3], dtype = np.uint8)
                self.colorRange.append(lowColor2)
                self.colorRange.append(highColor2)
                
            print("Color del componente: ",colorPick)

        else:
            print("Color no encontrado en la base de datos (Color Chart)")



    def __getColor__(self):
        return self.colorRange

             
        ## Para Debug ##
    def getRect(self):
        return [self.x1, self.y1, self.x2, self.y2]
    
    
    