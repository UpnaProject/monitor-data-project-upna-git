from AbstractDisplay import AbstractDisplay
import pytesseract

class NormalDisplay(AbstractDisplay):
    
    def __init__(self, dataComp):
        super().__init__(dataComp)


    def update(self, *args, **kwargs):
        self.result = pytesseract.image_to_string(super().__update__(*args),
                                             lang="eng", config="-psm 6 -oem 3 -c tessedit_char_whitelist=-+.0123456789")
   
    def getResult(self):
        return self.result