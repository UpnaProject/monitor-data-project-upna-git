import numpy as np
import cv2

class MainROI:
    
    def __init__(self, jsonMainRoi, bigPicture, pathMedia):
        
        tempCoordList = []
        sumList = []
        for coords in jsonMainRoi:
            tempCoordList.append((int(coords['x']), int(coords['y'])))
            tempSum = (int(coords['x'])+int(coords['y']))
            sumList.append(tempSum)        
        
        coordList = np.zeros((4, 2))
        index4Min = sumList.index(min(sumList))
        index4Max = sumList.index(max(sumList))
        coordList[0] = tempCoordList[index4Min]
        coordList[3] = tempCoordList[index4Max]
        if (index4Max > index4Min):
            del tempCoordList[index4Max]
            del tempCoordList[index4Min]
        else:
            del tempCoordList[index4Min]
            del tempCoordList[index4Max]
        
        if (tempCoordList[0][1:] < tempCoordList[1][1:]):
            coordList[1] = tempCoordList[0]
            coordList[2] = tempCoordList[1]
        else:
            coordList[1] = tempCoordList[1]
            coordList[2] = tempCoordList[0]           
        
        ## TODO Check Angle para saber inclinación ##
        ## Además, por ahora se queda con anchura la menor entre x2-x1 || x4-x3
        ## La altura se queda la menor de y3-y1 || y4-y2
    
        fWidth = coordList[1][:1] - coordList[0][:1] if (coordList[1][:1] - coordList[0][:1]) < (coordList[3][:1] - coordList[2][:1]) else (coordList[3][:1] - coordList[2][:1])  
        fHeight = coordList[2][1:] - coordList[0][1:] if (coordList[2][1:] - coordList[0][1:]) < (coordList[3][1:] - coordList[1][1:]) else (coordList[3][1:] - coordList[1][1:])  
        width = int(fWidth)
        height = int(fHeight)
        
        ## x1, y1 - x2, y2 - x3, y3 - x4, y4 ##
        pts1 = np.float32([[coordList[0][:1],coordList[0][1:]],[coordList[1][:1],coordList[1][1:]],[coordList[2][:1],coordList[2][1:]],[coordList[3][:1],coordList[3][1:]]])
        pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])
         
        ## Se obtiene Matriz perspectiva
        perspectiveMatrix = cv2.getPerspectiveTransform(pts1,pts2)
        ## Se aplica dicha matriz a la imagen y se ajusta  a las dimensiones calculadas. 
        self.dst = cv2.warpPerspective(bigPicture,perspectiveMatrix,(width,height))
         
        cv2.imwrite(pathMedia+"cropCapture.jpg",self.dst)  ##"outputBigPicture.jpg"



