from picamera.array import PiRGBArray
from picamera import PiCamera

class CameraRasp():
    
    
    def __init__(self):
        self.startCamera()

    def startCamera(self):
        self.camRasp = PiCamera()
        self.rawCapture = PiRGBArray(self.camRasp)
        # Se comprueba si la inicialización ha sido correcta
        if (self.camRasp.isOpened() == True):
            self.recording = True
            self.frameCapturado = []
        else:
            print("Cámara no inicializada")

    def getFrame(self):
        if (self.recording):
            self.camera.capture(self.rawCapture, format="bgr")
            currentFrame = self.rawCapture.array
            self.frameCapturado = currentFrame
        else:
            self.frameCapturado = self.frameCapturado
        return self.frameCapturado

    def stopCamera(self):
        if (self.recording):
            self.recording =not self.recording
            self.camPi.release()