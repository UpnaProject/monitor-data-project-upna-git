import cv2
import numpy as np
import os
from globalsProject import paths
from Helpers import saveToDisk, timeHelper
import settings

kernel = np.ones((3,3),np.uint8)
heightREF = 100 #in pixels
clahe = cv2.createCLAHE(clipLimit = 2, tileGridSize = (8,8))
itera = 1

def getContourns(tempRoi, nameComponent):
    roiList = []
    maxAreaCnt = 0
    tempRoiCopy = tempRoi.copy()
#     # Creamos canvas para almacenar contornos. Se convierte imagen a escala grises 
    canvas = np.zeros(tempRoi.shape, np.uint8)
    img2gray = cv2.cvtColor(tempRoi,cv2.COLOR_BGR2GRAY)
    roiG = cv2.GaussianBlur(img2gray, (5, 5), 0)
    roiG = clahe.apply(roiG)
    roiG = cv2.threshold(roiG, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    
    areaImg = tempRoi.shape[0] * tempRoi.shape[1]
    xMin = 0
    yMin = 0
    xMax = tempRoi.shape[1]
    yMax = tempRoi.shape[0]
    opening = cv2.morphologyEx(roiG, cv2.MORPH_OPEN, kernel)

#     # Extracción de contornos
    im2,contours,hierarchy = cv2.findContours(opening.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    
    contours = sorted(contours, key=cv2.contourArea, reverse=True)
    mask = np.zeros((tempRoi.shape[0], tempRoi.shape[1], 1), dtype=np.uint8)
    img4Mask = tempRoi.copy()

    positionXList = []
    index = 0
    for cnt in contours:
                
        x,y,w,h = cv2.boundingRect(cnt)
        if (x > 0 and y > 0 and x+w < xMax and y+h < yMax): #Si los contornos no tocan los bordes
            if (h + h/2) > w:
                if h > w:   #Single candidate
                    areaCNT = w * h
                    if areaCNT > maxAreaCnt * 0.35:
                        roi = tempRoi[y:y+h, x:x+w]
                        cv2.rectangle(tempRoi,(x,y),( x + w, y + h ),(0,255,0),2)
                        index = index +1
                        roiList.append(roi)
                        positionXList.append(x)

                        """ DEBUG_MODE INI """
                        if settings.DEBUG_MODE:
                            saveToDisk.save(
                                "_SingleNumber_"+str(nameComponent)+"_"+str(index),
                                tempRoiCopy[y:y+h, x:x+w], 
                                timeHelper.setTimeStamp(), 
                                paths.SAVING_CROPPED_NUMBERS)
                        """ DEBUG_MODE END """

                        cv2.drawContours(mask, [cnt], -1, (255, 255, 255), -1)
                        text_only = cv2.bitwise_and(img4Mask, img4Mask, mask=mask)
                        if areaCNT > maxAreaCnt:
                            maxAreaCnt = areaCNT

                elif (w >= h):   #Combined candidates
                    areaCNT = w * h
                    if areaCNT > areaImg * 0.05:
                        width = int(w/2)
                        roi1 = tempRoi[y:y+h, x:x+(width)-1]
                        roi2 = tempRoi[y:y+h, x+(width): x+w]
                        roiList.append(roi1)
                        roiList.append(roi2)
                        positionXList.append(x)
                        positionXList.append(x+width)
                        index = index +1

                        """ DEBUG_MODE INI """
                        if settings.DEBUG_MODE:
                            st = timeHelper.setTimeStamp()
                            saveToDisk.save(
                                "_CombinedNumber1_"+str(nameComponent)+"_"+str(index),
                                tempRoi[y:y+h, x:x+(width)-1], 
                                st, 
                                paths.SAVING_CROPPED_NUMBERS)
                            saveToDisk.save(
                                "_CombinedNumber2_"+str(nameComponent)+"_"+str(index),
                                tempRoi[y:y+h, x:x+(width)-1], 
                                st, 
                                paths.SAVING_CROPPED_NUMBERS)
                        """ DEBUG_MODE END """

        sortedRoiList = sortRois(positionXList, roiList)
    return sortedRoiList

def sortRois(listX, listRois):
    if len(listRois) > 0:
        listX, listRois = (list(t) for t in zip(*sorted(zip(listX, listRois))))        
    return listRois