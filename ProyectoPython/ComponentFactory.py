from Button import Button
from NormalDisplay import NormalDisplay
from Seg7Display import Seg7Display
import cv2

class ComponentFactory():

    typeComponent = ["Button", "Display", "7SegDisplay"]
    dataComponentList = []
    compList = []
    itera = 0
    def __init__(self, dataComponentList, refImage, modelNN):
        self.dataComponentList = dataComponentList
        self.refImage = refImage

        for comp in dataComponentList:
            try:
                if (comp.tipoComp == self.typeComponent[0]):
                    self.compList.append(Button(comp))
                elif (comp.tipoComp == self.typeComponent[1]):
                    self.compList.append(NormalDisplay(comp))
                elif (comp.tipoComp == self.typeComponent[2]):
                    self.compList.append(Seg7Display(comp, modelNN))
            except:
                raise NameError("Error al crear el componente. En ComponentFactory.class")



    def getResult2Send(self):
        dictDatos = {} 
        arrayDict = []
        for componentesCreados in self.compList:
            dictDatos = {'Componente':componentesCreados.name, 'Valor' : componentesCreados.getResult()}
            arrayDict.append(dictDatos)
        return arrayDict


    
    """ Debug_Mode """
    def drawOverlay(self,image):
        self.itera = self.itera +1
        font = cv2.FONT_HERSHEY_SIMPLEX
        for componentesCreados in self.compList:
            valor = componentesCreados.getResult()
            coord = componentesCreados.getRect()
            cv2.rectangle(image,(coord[0], coord[1]),(coord[2], coord[3]),(0,255,0),3)
            cv2.putText(image, valor, (coord[1],coord[2]),font, 1, (0,0,255), 4, cv2.LINE_AA)
        return image