import cv2
from PIL import Image
from abc import ABCMeta, abstractmethod
import numpy as np
import math

itera = 0
heightREF = 28 #in pixels. All rois should be resized to this size. NN was trained with these dimensions
max_x = heightREF
max_y = heightREF
num_Pixels = heightREF * heightREF

def resizeRois(listRois):
    data_image = np.zeros(shape = ((len(listRois), num_Pixels)), dtype=int)

    for index, roi in enumerate(listRois):
        heightT, widthT = roi.shape[:2]
        h_Relation = heightREF / heightT
        res = cv2.resize(roi,(0,0),fx=h_Relation, fy=h_Relation, interpolation = cv2.INTER_CUBIC)
        roiG = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

        canvas = np.zeros((max_x, max_y, 3), np.uint8)
        canvas = cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY)
    
        x_offset = math.floor((max_x - roiG.shape[1]) / 2)
        y_offset = 0
        canvas[y_offset:y_offset+roiG.shape[0], x_offset:x_offset+roiG.shape[1]] = roiG
        heightCanvas, widthCanvas = canvas.shape[:2]
        data_image[index] = canvas.reshape(1,heightCanvas*widthCanvas)
    return data_image