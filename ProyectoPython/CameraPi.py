import cv2
import os

class CameraPi():


    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.startCamera()
        

    def startCamera(self):
        self.camPi = cv2.VideoCapture(0)
        self.camPi.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        self.camPi.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)
        self.frameCapturado = []
        # Se comprueba si la inicialización ha sido correcta
        if (self.camPi.isOpened() == True):
            self.recording = True
            print("Cámara Inicializada")          
        else:
            print("Cámara no inicializada")
            raise Exception("Cámara no inicializada. Por favor, compruebe que hay una cámara conectada al dispositivo y que tiene los permisos necesarios --> sudo modprobe bcm2835-v4l2")

    def getFrame(self):
        if (self.recording):
            existeFrame, currentFrame = self.camPi.read()
            self.frameCapturado = currentFrame            
        else:
            self.frameCapturado = self.frameCapturado
        return self.frameCapturado

    def stopCamera(self):
        if (self.recording):
            self.recording =not self.recording
            self.camPi.release()
