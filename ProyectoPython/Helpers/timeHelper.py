import time
import datetime

def setTimeStamp():
    timeS = time.time()
    ts = datetime.datetime.fromtimestamp(timeS).strftime('%Y-%m-%d %H.%M.%S')
    return ts