import cv2
import FirstImage
from FirstRun import FirstRun
from CameraPi import CameraPi
from ISubject import ISubject
from JsonToPython import JsonToPython
from DataComponentFactory import DataComponentFactory
from ComponentFactory import ComponentFactory
from Homography import Homography
from MainROI import MainROI
from Data2Send import Data2Send
from CalibrationHelper import CalibrationHelper
from NeuronalNetwork import NeuronalNetwork
import pygame
import os
from globalsProject import paths
import numpy as np
from Helpers import timeHelper, saveToDisk
import settings

if __name__ == "__main__":
    """ PATHS """ ## Configure them in paths.py file ##
    inputJsonPath = paths.INPUT_JSON
    resourcesPath = paths.RESOURCES
    mediaPath = paths.MEDIA
    debugImagesPath = paths.SAVING_DEBUG_ROIS
    """ SETTINGS """ ##Configure them in settings.py file ##
    DEBUG_MODE = settings.DEBUG_MODE
    CAMERA_CALIBRATION = settings.CAMERA_CALIBRATION
    UPDATE_LOOP = settings.UPDATE_LOOP


    PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
    BASE_DIR = os.path.dirname(PROJECT_ROOT)

    calibration = CalibrationHelper()
    jasonHandler = JsonToPython(inputJsonPath)
    filenameI= mediaPath + 'capture.jpg'

    if CAMERA_CALIBRATION:
        cc = FirstImage(filenameI)
        cc.setFirsRun()
        baseImageCalibrated = calibration.getUndistortedImage(cc.getBaseImage())
        saveToDisk.save('capture', baseImageCalibrated, '', mediaPath)


    neuronalModel = NeuronalNetwork(PROJECT_ROOT + resourcesPath +'UPNA.h5')
    neuronalModel.getInfoTest()

    fR = FirstRun(jasonHandler, inputJsonPath)
    fR.setFirsRun()
    refImage = cv2.imread(filenameI)
    height, width, channels = refImage.shape

    refImage = calibration.getUndistortedImage(refImage) if CAMERA_CALIBRATION else refImage

    ## Se obtiene el MainRoi que se enviará por Wifi al dispositivo. Este devolverá un json con los componentes
    mainROI = MainROI(jasonHandler.jsonLoad(inputJsonPath+'puntos.json'), refImage, mediaPath)  ## outputBigPicture.jpg como salida que se guardará en el root
    fR.setSecondRun()

    # Se recoge el Json y se procesa para convertirlo a datos usables.
    json_data = jasonHandler.jsonLoads(open(inputJsonPath+'regiones.json').read())

    d2c = DataComponentFactory(json_data)
    componentFactory = ComponentFactory(d2c.dataComponentList, mainROI.dst, neuronalModel)

    cameraHandler = CameraPi(width, height)

    subject = ISubject()
    for c in componentFactory.compList:
        subject.register(c)
    subject.ini_observers(mainROI.dst)


    homographyCalculator = Homography(mediaPath)
    data2Send = Data2Send(jasonHandler)

# ===========================FIN Inicialización================================
    
# ======================Métodos llamados en Update=============================

    def updateCurrentFrame():
        return calibration.getUndistortedImage(cameraHandler.getFrame()) if CAMERA_CALIBRATION else cameraHandler.getFrame()

    def drawRects():
        return componentFactory.drawOverlay(homographyCalculator.im_out)
     
    def updateDelivery(currentTimeStamp):
        data2Send.update(componentFactory.getResult2Send(), currentTimeStamp)

    def update():
        currentTimeStamp = timeHelper.setTimeStamp()
        currentFrame = updateCurrentFrame()
        homographyCalculator.update(currentFrame)
        if homographyCalculator.im_out.any():
            subject.update_observers(homographyCalculator.im_out)
        updateDelivery(currentTimeStamp)

    """ DEBUG_MODE LOOP"""
    def update_Debug():
        currentTimeStamp = timeHelper.setTimeStamp()
        currentFrame = updateCurrentFrame()
        homographyCalculator.update(currentFrame)
        if homographyCalculator.im_out.any():
            subject.update_observers(homographyCalculator.im_out)
        saveToDisk.save(' 1_RecordedFrame', currentFrame, currentTimeStamp, debugImagesPath)
        saveToDisk.save(' 2_Homography', homographyCalculator.im_out, currentTimeStamp, debugImagesPath)    
        saveToDisk.save(' 4_VisualResults', drawRects(), currentTimeStamp, debugImagesPath)
        updateDelivery(currentTimeStamp)


# ============================UPDATE LOOP======================================

    import os
    os.environ["SDL_VIDEODRIVER"] = "dummy"

    import pygame.display
    pygame.display.init()
    screen = pygame.display.set_mode((160,90))

    loopUpdate = pygame.time.Clock()
    boolLoop = False
    try:
        while not boolLoop:
           update() if not DEBUG_MODE else update_Debug()

           for event in pygame.event.get():
               if event.type==pygame.QUIT or event.type==pygame.KEYDOWN:
                   boolLoop = True
                   pygame.quit()

           loopUpdate.tick(UPDATE_LOOP)
        else:
            pygame.quit()
            cameraHandler.stopCamera()
    except KeyboardInterrupt:
        pass

# =============================================================================

