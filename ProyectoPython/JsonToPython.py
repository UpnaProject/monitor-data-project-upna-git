import json
try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Opción alternativa para Python 2.x
    from argparse import Namespace
            
class JsonToPython():

    
    def __init__(self, pathJson):
        self.pathJson = pathJson
        pass       
        
    def jsonLoad(self, jsonLoad):
        self.json_Load = json.load(open(jsonLoad))
        return self.json_Load

    def jsonLoads(self, jsonLoads):
        self.json_Loads = json.loads(jsonLoads, object_hook=lambda d: Namespace(**d))
        return self.json_Loads
        
    def toJson(self, data2Json, timestamp):
        with open(self.pathJson+timestamp+'_output.json', 'w') as fp:
            json.dump(data2Json, fp)
        return json.dumps(data2Json)