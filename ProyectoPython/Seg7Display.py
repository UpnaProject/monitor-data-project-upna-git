from AbstractDisplay import AbstractDisplay
import pytesseract
from Helpers import saveToDisk
from NumberExtractor import getContourns
from FitImagesToNN import resizeRois
import numpy as np
import settings
from globalsProject import paths
from Helpers import saveToDisk, timeHelper
import settings

class Seg7Display(AbstractDisplay):
    
    def __init__(self, dataComp, modelNN):
        super().__init__(dataComp)
        self.name = dataComp.nombreComp
        self.modelNN = modelNN

    def update(self, *args, **kwargs):
        tempRoi = super().__getRoi__(*args)
        """ DEBUG_MODE INI """
        if settings.DEBUG_MODE:
            saveToDisk.save(
                "_ROI_"+str(self.name),
                tempRoi, 
                timeHelper.setTimeStamp(), 
                paths.SAVING_CROPPED_ROIS)
        """ DEBUG_MODE END """               
         
        roiList = getContourns(tempRoi, self.name)
        resizeRoisList = resizeRois(roiList)
        stringPrediction = []
        if len(resizeRoisList) > 0:
            for number in resizeRoisList:
                prediction = self.modelNN.predictResults(number)
                stringPrediction.append(str(prediction))
            res = ''.join(stringPrediction)
            remove_LeftBrack = res.replace("[", "")
            remove_RightBrack = remove_LeftBrack.replace("]", "")
            print('Character Prediction',remove_RightBrack)

            self.result = remove_RightBrack ## TODO
        else:
            tempImage = super().__getImageFromROI__(tempRoi)
            self.result = pytesseract.image_to_string(tempImage,
                                                lang="letsgodigital", config="-psm 6 -oem 3 -c tessedit_char_whitelist=-+.0123456789")

    def getResult(self):
        return self.result