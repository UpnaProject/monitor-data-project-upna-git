The scope of this UPNA project comprises the implementation of an algorithm 
which extracts the useful info from an input image (industrial displays) 
and get the results after applying image proccesing and OCR techniques.


The entry point to launch the project is the app.py file.
>>> python app.py

It is mandatory to custom the non-relative paths to store the info provided to the software and so its output.
The file is paths.py
The example given shows how the configuration was done in a raspberry, taken into account the several folders needed.
Only the INPUT_JSON and MEDIA are required in normal mode.
The rest of them are required if DEBUG mode is active.

    """ EXAMPLE PATHS FOR RASPBERRY  """ ## This should be removed after the CUSTOM PATHS is defined ##
    INPUT_JSON= '/var/www/html/upload/'
    MEDIA = '/var/www/html/media/'
    SAVING_DEBUG_ROIS = '/var/www/html/media/'
    SAVING_CROPPED_NUMBERS = '/var/www/html/media/croppedNum'
    SAVING_CROPPED_ROIS = '/var/www/html/media/croppedROI'

    """ CUSTOM PATHS """ ## INTRODUCE Here the paths for the current project ##
    # INPUT_JSON =
    # MEDIA = 
    # SAVING_DEBUG_ROIS =
    # SAVING_CROPPED_NUMBERS = 
    # SAVING_CROPPED_ROIS = 


The settings.py allow to minimally configure some params, as the DEBUG mode, use of calibration camera or frequency of the main loop. 